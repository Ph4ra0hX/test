module.exports = {
  "Demo test": function(browser) {
    browser
      .url("https://nightwatch-demo.netlify.com/")
      .waitForElementVisible("[data-nw=name-input]")
      .setValue("[data-nw=name-input]", "Wesley")
      .pause(1000)
      .assert.containsText("[data-nw=welcome-message]", "Wesley")
      .end();
  }
};
